import matplotlib.pyplot as plt
import os
import re

X,Y,Z = [], [], []

def avg_from_file(string):
    rc = string                         #The file
    string = string.split("_0_")[0]     #Get the file Name (prefix)
    demands = string+"_0.demands"       #Get the .demands file name
    graph = string+".graph"             #Get the .graph file name
    counter = 0
    listdemands = []
    listgraph = []
    pattern2 = re.compile("demand_*")   #regex for demand_X lines
    pattern3 = re.compile("edge_*")     #regex for edge_X lines 
    for line in open("../dem_prop/"+demands,'r'):   #open demands file
        values = [s for s in line.split(" ")]       #split the line 
        if pattern2.match(values[0]) != None:       #if it's a demand line
            listdemands.append((int(values[1]),int(values[2]),float(values[3].split("\n")[0]))) #save the values
    for line in open("../../graphs/"+graph,'r'):    #open graph file
        values = [s for s in line.split(" ")]       #split the line
        if pattern3.match(values[0]) != None:       #if it's a edge line
            listgraph.append((int(values[1]),int(values[2]),int(values[4]))) #save the values
    blacounter = 0
    bigcounter = 0
    realcounter  = 0
    for line in open(rc, 'r'):          #open RC file
        counter = counter + 1           #count the different routs
        if counter == 1 :               #remove first line
            continue
        realcounter = realcounter + 1   #count the routing solutions
        values = [float(s) for s in line.split(",")]    #split the line
        for i in listdemands:           #save the bw needed to a pair
            (src,dst,bwNeeded) = i
            if (src == values[0] and dst == values[-2]):
                break
        bwNeeded = bwNeeded + ((bwNeeded*50)/100)
        if values[-1] >= 0.9:           #If bw > 0.7
            bigcounter = bigcounter+1   #count them
        bwMin = 10000000000
        for i in range(0,len(values)-2):
            for j in listgraph:
                (src,dst,bw) = j        #retrieve the tuple
                if (src == int(values[i])) & (dst == int(values[i+1])):
                    bwMin = min(bwMin, bw) #if we found src & dest then check min
            if float(bwNeeded) > float(bwMin):
                blacounter = blacounter + 1 #if needed is > min
    Y.append(float(blacounter)/float(realcounter)) #Congestion
    X.append(float(bigcounter)/float(realcounter) - 0.2)

def countHOPS(string):
    count = 0
    counter = 0
    for line in open(string, 'r'):
        counter = counter + 1           #count the different routs
        if counter == 1 :               #remove first line
            continue
        values = [float(s) for s in line.split(",")]    #split the line
        count = count + len(values)-2
    return count

def lat_str(string,str2):
    string = string.split("_0_")[0]
    ch1 = countHOPS(string+"_0_"+str2+".rc")
    ch2 = countHOPS(string+"_0_SP.rc")
    Z.append(float(ch1)/float(ch2))

files = [f for f in os.listdir('.') if os.path.isfile(f)]

def computeGraph(locality,inp,savePath,showPlt):
    pattern = re.compile(".*_0_"+inp+".rc")
    ctr = 0
    del X[:]
    del Y[:]
    del Z[:]
    for f in files:
        if pattern.match(f) != None:
            ctr = ctr+1
            print(str(int(((float(ctr))/116)*100)) + "%")
            avg_from_file(f)
            lat_str(f,inp)

    plt.clf()
    plt.title(inp)
    plt.ylabel('fraction of pairs congested')
    plt.xlabel('LLPD')
    plt.plot(X, Y,'ro')
    plt.savefig(savePath+locality+"_"+inp+"_f.png")
    if(showPlt):
        plt.show()

    plt.clf()
    plt.title(inp)
    plt.ylabel('latency stretch')
    plt.xlabel('LLPD')
    plt.plot(X, Z,'ro')
    plt.savefig(savePath+locality+"_"+inp+"_l.png")
    if(showPlt):
        plt.show()

computeGraph("0","B4","/Users/alexanderkatsarakis/Desktop/testtt/",False)
computeGraph("0","LDR","/Users/alexanderkatsarakis/Desktop/testtt/",False)
computeGraph("0","LDRNFC","/Users/alexanderkatsarakis/Desktop/testtt/",False)
computeGraph("0","LDRNOCACHE","/Users/alexanderkatsarakis/Desktop/testtt/",False)
computeGraph("0","MinMaxK10","/Users/alexanderkatsarakis/Desktop/testtt/",False)
computeGraph("0","MinMaxLD","/Users/alexanderkatsarakis/Desktop/testtt/",False)