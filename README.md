## Project Description

A script that parses results of the traffic matrix generator tool [tm-gen](https://github.com/ngvozdiev/tm-gen) in order to calculate <br/>

- LLPD
- Number of pairs Congested
- Latency strech


### Running information

 
<b> 1. First you need to run the traffic matrix generation tools. </b><br>
You can find them here: https://github.com/ngvozdiev/tm-gen </br>
Sample commands:<br><br>
    - `docker run -v /tmp/demand_matrices:/demand_matrices ngvozdiev/tm-gen --locality=0.93` <br>
    - `docker run -v /tmp/demand_matrices:/demand_matrices --entrypoint tm-run ngvozdiev/tm-gen` </br></br>
    Note: The correct arguments to run tm-gen is --locality=1 and --tm_count=100 as the authors state in their paper. However these commands crash the docker image when executing tm-run. 


<b> 2. Save the result as follows:</b><br>
```/graphs                   (.graph files must be in here)``` <br>
```/results``` <br>
```/results/locality_0.93    (.rc files and python script must be in here)```  <br>
```/results/dem_prop         (.demands/.properties files must be in here)```<br>

  
<b> 3. Open the python script and change at the lines (102-107) and include the path you want.  </b><br>
    e.g. computeGraph("0.93","B4","where_to_save_figures_path",False)


<b> 4.  Run the script:  </b> <br>
    `python tm-gen-parser.py`

### Authors

- Manos Adamakis
- Dionisis Kalochristianakis
- Alexandros Katsarakis

